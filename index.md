---
layout: default
title: Home Page
---

<nav>
  <a href="#">Hjem</a>
  <a href="#">Bilder av Arbeid</a>
  <a href="#">Om Oss</a>
</nav>
<h1>30 år med kvalitet!</h1>
<div>
  <button>Ring Oss</button>
  <button>E-Post</button>
</div>

<section>
  <h2>Tjenester</h2>
  <p>Vi utfører alle typer sprengningsarbeid som tomter, veier, grofter og tilbygg.</p>
</section>

<section>
  <h2>Historie</h2>
  <p>Østenstads Sprengningsservice AS ble startet i 1986 av Hallgeir Østenstad. Vi består i dag av 5 mann og 3 hydrauliske borriggere. Og egen bil for transport av utstyr.</p>
</section>

<section>
  <h2>Kvalitet</h2>
  <p>Siden starten er det to ting som har drevet oss og som gjør at vi stikke litt mer ut: Det er kvalitet og pålitelighet. Vi har stolthet i det vi gjør og belønningen får vi gjennom fornøyde kunder og gode samarbeidspartnere.</p>
</section>

<footer>
  <p>sprengningsservice.no - Drammensveien 973, 1385 Asker org. nr. NO 978642926 MVA</p>
  <p>416 10 455 - 952 85 519</p>
</footer>
